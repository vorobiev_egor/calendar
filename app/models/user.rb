class User < ActiveRecord::Base
  has_many :events,dependent: :destroy

  validates :email,:password,:password_confirmation,presence: true
  validates :password,confirmation: true

  validate :email,format: {with: Email.regexp}
  validates_uniqueness_of :email
end
