class EventsController < ApplicationController
  before_action :event_belongs_to_user?,only: [:update,:destroy]
  before_action :current_user,only: [:create]

  def create
   @event = Event.new(event_params) 
   @event.user = @user

   if @event.save
     redirect_to home_path
   else
     render "users/show"
   end
  end

  def edit
    @event = Event.find_by(id: params[:id])
  end
  
  def update
   if @event.update_attributes(event_params)
     redirect_to user_path
   else
     render :edit
   end
  end

  def destroy
    @user.events.destroy(params[:id]) if @user.events.find_by(id: params[:id]) 
    redirect_to home_path
  end

  protected
    def event_params
      params.require(:event).permit(:name,:start_time,:repeat)
    end

    def event_belongs_to_user?
      redirect_to home_path unless @event = @user.events.find_by(id: params[:id]) 
    end
end
