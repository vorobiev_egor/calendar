class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.references :user
      t.datetime :data
      t.string :name
      t.string :repeat

      t.timestamps
    end
  end
end
